# recipe-app-api-proxy
# Recipe APP API Proxy
Nginx Proxy app for our recipe app API

##Usage

###Environment Variables

* `LISTEN_PORT` - port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app tp forward requests to (default: `9000`)
